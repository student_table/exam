
const express = require('express')

const routerPerson = require('./Routes/person')

const app = express();

app.use('/person',routerPerson)
app.use(express.json)

app.listen(4000,'0.0.0.0',()=>{
    console.log('server started')
})